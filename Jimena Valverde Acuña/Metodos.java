//Calcule el índice de masa corporal de una persona (IMC=kg/m2, masa entre altura al cuadrado)
//e imprime el estado en que se encuentra esa persona de acuerdo a lo siguiente:
//Si el IMC es menor a 16 imprimir el mensaje que diga Criterio de ingreso al hospital,  
//indique si tiene Obesidad mórbida si el IMC es mayor a 40, si es menor a 40 pero mayor o igual a 35 tiene Obesidad premórbida
//y si es menor a 35 pero mayor o igual a 30 tiene Sobrepeso crónico, 
//un IMC menor que 30 pero mayor o igual a 25 tiene sobrepeso, menor a 25 y mayor o igual a 18 está saludable, 
//menor a 18 pero mayor o igual a 17 tiene bajo peso, menos de 17 pero mayor o igual a 16 tiene infrapeso.


//Clase metodos 

public class Metodos
{
	private double peso;
	private double altura;
	private double imc;
	
	
	public String calcularImc ( double peso, double altura)
	{
		imc=peso/altura*altura;
		
		if (imc>40)
		{
			return " Usted tiene Obesidad morbida";
		}
		else
		{
			if (imc<40 || imc>=35)
			{
				return  "Usted tiene Obesidad premorbida";
			}
			else
			{
				if(imc<35 || imc>=30)
				{
					return  "Usted tiene Sobrepeso cronico";
				}
				else
				{
					if(imc<30 || imc>=25)
					{
						return  "Usted tiene sobrepeso";
					}
					else
					{
						if (imc<25 ||imc>=18)
						{
							return  "Usted esta con un peso saludable";
						}
						else
						{
							if (imc <18 || imc>=17)
							{
								return " Usted tiene bajo peso";
							}
							else
							{
								if(imc<17 || imc>=16)
								{
									return " Usted tiene infrapeso";
								}
								else
								{
									return " Criterio de ingreso al hospital";
								}
							}
						}		
					}
				}
			}
		}//fin if else
	
	}//fin calcular IMC
	
	
}//fin de la clase 
	
