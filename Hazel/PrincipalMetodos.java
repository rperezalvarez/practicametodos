import javax.swing.JOptionPane;

public class PrincipalMetodos {
	
	public static void main (String arg[]){
	
		Metodo metodoPromedio = new Metodo();
		
		String valorLeido;
		
		valorLeido=JOptionPane.showInputDialog("Digite la edad de la persona uno");	
		int edadUno;
		edadUno=Integer.parseInt(valorLeido);
		
		valorLeido=JOptionPane.showInputDialog("Digite la edad de la persona dos");	
		int edadDos;
		edadDos=Integer.parseInt(valorLeido);
		
		valorLeido=JOptionPane.showInputDialog("Digite la edad de la persona tres");	
		int edadTres;
		edadTres=Integer.parseInt(valorLeido);
		
		metodoPromedio.obtenerPromedio(edadUno, edadDos, edadTres);	
		
	}
	

		
}//fin
