public class Compra{
	double totalCompra;
	double dineroEmpresa;
	double prestamo;
	double credito;
	double interes;
	
public Compra(double totalCompra, double dineroEmpresa, double prestamo, double credito, double interes){
	this.totalCompra=totalCompra;
	this.dineroEmpresa=dineroEmpresa;
	this.prestamo=prestamo;
	this.credito=credito;
	this.interes=interes;
	}//Constructor
		
public void setTotalCompra(double totalCompra){
	this.totalCompra=totalCompra;
	}//Set
			
public double getTotalCompra(){
	return totalCompra;
	}//get
	
public void setDineroEmpresa(double dineroEmpresa){
	this.dineroEmpresa=dineroEmpresa;
	}//Set
			
public double getDineroEmpresa(){
	return dineroEmpresa;
	}//get			

public void setPrestamo(double prestamo){
	this.prestamo=prestamo;
	}//Set
			
public double getPrestamo(){
	return prestamo;
	}//get

public void setCredito(double credito){
	this.credito=credito;
	}//Set
			
public double getCredito(){
	return credito;
	}//get

public void setInteres(double interes){
	this.interes=interes;
	}//Set
			
public double getInteres(){
	return interes;
	}//get
		
public String compraEmpresa(double totalCompra){
	 if(totalCompra<=500000){
		dineroEmpresa=(totalCompra*55)/100;
		prestamo=(totalCompra*30)/100;
		credito=(totalCompra*15)/100;
		interes=(credito*20)/100;
			}else{
				dineroEmpresa=(totalCompra*70)/100;
				credito=(totalCompra*30)/100;
				interes=(credito*20)/100;
					}//if-else
		return "El total a pagar por la empresa es: "+dineroEmpresa+", el prestamo a solicitar es de: "+prestamo+", el credito a solicitar al fabricante es de: "+credito+" y el interes que la empresa cobrara sobre ese monto es de: "+interes;
	}//compraEmpresa
}//Fin de la clase
