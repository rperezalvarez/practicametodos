import javax.swing.JOptionPane;
/*En una clase defina y cree un arreglo unidimensional de tipo int de 20 elementos. Escriba
además las sentencias para:
a. Llenar el arreglo. Los valores deben ser generados de forma aleatoria. En un rango
definido entre 1 y 250 .
b. Mostrar los valores del arreglo.
c. Buscar un valor en el arreglo que el usuario haya digitado. Debe mostrar un mensaje
informando si lo encontró o no.
d. Mostrar la posición de un valor contenido en el arreglo. El valor a buscar va a ser
digitado por el usuario.
e. Mostrar el valor del elemento mayor almacenado en el vector. Asuma que no hay
valores repetidos.
f. Mostrar la posición del elemento menor.
g. Solicitar una posición y modificar el valor contenido en esa posición.*/

public class Enunciado2
{
	public static void main (String arg[])
	{
		int arreglo2[]=new int[20];
		int leer, numero, guardar, contador, numMayor, numMenor;
		String mostrarTodo;
		
		numMayor=0;
		numMenor=300;
		contador=0;
		guardar=0;
		mostrarTodo="Listado de arreglo1\n";
		
	//a. Llenar el arreglo. Los valores deben ser generados de forma aleatoria.
	//En un rango definido entre 1 y 250
		
		do
		{
			leer=(int)(Math.random()*250+1);
			arreglo2[contador]=leer;
			contador++;
		}while(contador<arreglo2.length);
		
	//b. Mostrar los valores del arreglo.
		
		for (contador=0; contador<arreglo2.length;contador++)
		{
			mostrarTodo+="Posicion "+contador+"= "+arreglo2[contador]+"\n";
		}//fin del for
		
		JOptionPane.showMessageDialog(null,mostrarTodo);
		
	//c. Buscar un valor en el arreglo que el usuario haya digitado. Debe mostrar un mensaje
		//informando si lo encontró o no.
	//d. Mostrar la posición de un valor contenido en el arreglo. El valor a buscar va a ser
		//digitado por el usuario.
		
		contador=0;//reiniciar contador
		
		leer=Integer.parseInt(JOptionPane.showInputDialog("Digite el numero que desea buscar"));
		do
		{
			if (arreglo2[contador]==leer)
			{
				guardar=contador;
			}//fin del if
			
			contador++;
		}while(contador<arreglo2.length);
		
		if (guardar!=0)
		{
			JOptionPane.showMessageDialog(null,"El numero "+leer+" se encuentra en la casilla "+arreglo2[contador]);
		}//fin de if
		else
		{
			JOptionPane.showMessageDialog(null,"El numero digitado no se encuentra en ninguna casilla");
		}//fin del else
		
	//e. Mostrar el valor del elemento mayor almacenado en el vector. Asuma que no hay valores repetidos.
		
		 contador=0; //reiniciar el contador
		do
		{
			if(numMayor<arreglo2[contador])
			{
				numMayor=arreglo2[contador];
			}//fin del if
			contador++;
		}while(contador<arreglo2.length);
		
		JOptionPane.showMessageDialog(null,"El numero mayor en el arreglo es "+numMayor);
		
	//f. Mostrar la posición del elemento menor.
		
		 contador=0; //reiniciar el contador
		do
		{
			if(arreglo2[contador]>numMenor)
			{
				numMenor=arreglo2[contador];
			}//fin del if
			contador++;
		}while(contador<arreglo2.length);
		
		JOptionPane.showMessageDialog(null,"El numero menor en el arreglo es "+numMenor);
		
	//g. Solicitar una posición y modificar el valor contenido en esa posición.
		
		leer=Integer.parseInt(JOptionPane.showInputDialog("Digite la posicion del arreglo que desea modificar"));
		numero=leer;
		leer=Integer.parseInt(JOptionPane.showInputDialog("Digite la posicion del arreglo que desea modificar"));
		guardar=arreglo2[leer];
		arreglo2[leer]=numero;
		
		JOptionPane.showMessageDialog(null,"Cambio de "+guardar+" a "+arreglo2[leer]+" realizado con exito");
		
	}//fin del main
}//fin de la clase
