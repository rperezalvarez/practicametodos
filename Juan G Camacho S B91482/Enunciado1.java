import javax.swing.JOptionPane;
/*1. En una clase defina y cree un arreglo unidimensional de tipo double capaz de almacenar 10
valores. Escriba además las sentencias necesarias para:
a. Llenar el arreglo.
b. Calcular e imprimir el promedio de los valores almacenados en el arreglo.
c. Determinar cuántos valores son mayores que el promedio.
d. Determinar cuáles valores son menores que el promedio.
e. Mostrar los valores del arreglo.*/

public class Enunciado1
{
	public static void main (String arg[])
	{
		double arreglo1[]=new double[10];
		String numMayor, numMenor, mostrarTodo;
		double leer, promedio, total;
		int contador;
		
		total=0.0;
		contador=0;
		mostrarTodo="Listado de arreglo1\n";
		numMayor="Lista de numeros \n";
		numMenor="Lista de numeros \n";
		
		//a. Llenar el arreglo.
		do
		{
			leer=Double.parseDouble(JOptionPane.showInputDialog("Digite el numero para la pocision "+contador));
			arreglo1[contador]=leer;
			contador++;
		}while(contador<arreglo1.length);
		
		//b. Calcular e imprimir el promedio de los valores almacenados en el arreglo.
		
		for (contador=0; contador<arreglo1.length;contador++)
		{
			total+=arreglo1[contador];
		}//fin del for
		
		promedio=(total/arreglo1.length);
		JOptionPane.showMessageDialog(null,"El promedio del los datos en el arreglo es "+promedio);
		
		//c. Determinar cuántos valores son mayores que el promedio.
		 
		 contador=0; //reiniciar el contador
		do
		{
			if(promedio<arreglo1[contador])
			{
				numMayor+="Posicion "+contador+"= "+arreglo1[contador]+"\n";
			}//fin del if
			contador++;
		}while(contador<arreglo1.length);
		
		JOptionPane.showMessageDialog(null,"Los numeros mayores al promedio son\n"+numMayor);
		
		//d. Determinar cuáles valores son menores que el promedio.
		
		 contador=0; //reiniciar el contador
		do
		{
			if(promedio>arreglo1[contador])
			{
				numMenor+="Posicion "+contador+"= "+arreglo1[contador]+"\n";
			}//fin del if
			contador++;
		}while(contador<arreglo1.length);
		
		JOptionPane.showMessageDialog(null,"Los numeros menores al promedio son\n"+numMenor);
		
		//e. Mostrar los valores del arreglo.
		
		for (contador=0; contador<arreglo1.length;contador++)
		{
			mostrarTodo+="Posicion "+contador+"= "+arreglo1[contador]+"\n";
		}//fin del for
		
		JOptionPane.showMessageDialog(null,mostrarTodo);
		
	}//fin del main
}//fin de la clase
