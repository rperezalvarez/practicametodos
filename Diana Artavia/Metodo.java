public class Metodo{
	private double radio;

public Metodo(double radio){
	this.radio=radio;
	}//fin del metodo constructor
	
		public void setRadio(double radio){
		this.radio=radio;
	}//fin del metodo setRadioUno
	
	public double getRadio(){
		return radio;
	}//fin del metodo getRadioUno
	
	
	public double calcularArea(double pi, double radio){
		return pi*(radio*radio);
	}//fin del metodo calcularArea

}// fin de la clase Metodo
