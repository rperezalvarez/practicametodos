import javax.swing.JOptionPane;

public class Principal{
	public static void main (String arg[]){
		double radio;
		Metodo areaCirculo;
		areaCirculo= new Metodo(5.5);
	
		//JOptionPane.showMessageDialog(null, "El area del circulo es "+areaCirculo.calcularArea(3.14,5.5));
	
		radio=Double.parseDouble(JOptionPane.showInputDialog("Digite el radio del circulo"));
		areaCirculo.setRadio(radio);
		JOptionPane.showMessageDialog(null, "El area del circulo es "+areaCirculo.calcularArea(3.14,radio));
		
	}//fin del main 
}//fin de la clase Principal
