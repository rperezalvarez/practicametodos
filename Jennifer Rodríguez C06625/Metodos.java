import javax.swing.JOptionPane;
public class Metodos {
	public static void main(String[] args) {
		String str = "";
		str = JOptionPane.showInputDialog ("Escriba la palabra o frase a ser invertida");
		System.out.println(str);
		StringBuilder strb = new StringBuilder(str);
		str = strb.reverse().toString();
		System.out.println(str);
	}//Fin del main
}//Fin de la clase
