//Escriba método que reciba una palabra  (String) y devuelva el número de vocales que tiene, haga uso del método anterior//

public class ConteoDeVocales{
	
	public int contarVocales(String palabra){
		int contarVocales=0;
		
		for (int posicion = 0; posicion < palabra.length(); posicion++){
			if ((palabra.charAt(posicion)=='a') || (palabra.charAt(posicion)=='e')||(palabra.charAt(posicion)=='i')||
			( palabra.charAt(posicion)=='o')||(palabra.charAt(posicion)=='u')){
				contarVocales++;
				}//Fin del if
			}//Fin del For 
			return contarVocales;
	
		}//fin del metodo

}//fin de la clase

